function load-secrets {
    echo "📣 Loading tokens and passwords 🔓"
    source <(sudo cat /etc/env)
}

function load-ssh-key {
    echo "📣 Loading SSH key 🔓"
    eval `ssh-agent -s`
    ssh-add ~/.ssh/gs_id_rsa
}
