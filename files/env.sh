#
# Load work environment
#
function load-work-environment() {
    #
    # Load secrets
    #
    load-secrets

    #
    # Load SSH key
    #
    load-ssh-key

    #
    # Start VPN
    #
    start-vpn
}
