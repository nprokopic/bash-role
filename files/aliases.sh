# kubectl
alias k=kubectl
alias kg='kubectl get'
alias kga='kubectl get --all-namespaces'
alias kgn='kubectl get -n giantswarm'
alias kgd='kubectl get -n default'

# gsctl
alias g='gsctl'
alias gl='g list'
alias glc='gl clusters'
alias glr='gl releases'
alias gle='gl endpoints'
