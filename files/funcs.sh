#!/bin/bash

function goto() {
    installation=$1
    tenant_cluster=$2

    gsctl select endpoint $installation

    if [ -z "$tenant_cluster" ]; then
        
        echo "☸️ Creating kubeconfig for $installation"

        opsctl create kubeconfig -i "$installation"

    else
        gsctl create kubeconfig --cluster=$tenant_cluster --certificate-organizations system:masters
    fi

    case $installation in

        ghost | godsmack | gollum)
            echo "📣 Setting GiantSwarm Azure subscription for Azure CLI 🏁"
            az account set -s $GIANTSWARM_AZURE_SUBSCRIPTION
            ;;
        
        *)
            echo "⛔ Azure subscription not set for Azure CLI."
            ;;
    esac
}

function where() {
    echo "☸️ k8s: $(__get_kubernetes_prompt)"
}

function start-vpn() {
    connection=$1

    if [ -z "$connection" ]; then
        connection="gridscale"
    fi

    case $connection in

        gridscale | vultr)
            echo "📣 Starting $connection VPN 🌍🔐"
            opsctl vpn open --vpn-config-file="/home/nikola/giantswarm/vpn/nikola.$connection.2.ovpn"
            export __OPSCTL_VPN_CONNECTION=$connection
            ;;
        
        *)
            echo "⛔ VPN connection '$connection' not found."
            ;;

    esac
}

function stop-vpn() {
    if [ -z "$__OPSCTL_VPN_CONNECTION" ]; then
        echo "⛔ VPN connection is down or it's open in another terminal session. Is it gridscale or vultr? Close it manually. Try some of these:
    opsctl vpn close --vpn-config-file=/home/nikola/giantswarm/vpn/nikola.gridscale.2.ovpn
    opsctl vpn close --vpn-config-file=/home/nikola/giantswarm/vpn/nikola.vultr.2.ovpn"
        return
    fi

    connection=$__OPSCTL_VPN_CONNECTION

    case $connection in

        gridscale | vultr)
            echo "📣 Closing $connection VPN connection 🌍"
            opsctl vpn close --vpn-config-file="/home/nikola/giantswarm/vpn/nikola.$connection.2.ovpn"
            ;;
        
        *)
            echo "⛔ VPN connection '$connection' not found."
            ;;

    esac
}

function get() {
    target=$1

    case $target in

        giantswarm-tools)
            __ansible_install $target
            ;;
        
        gsctl | opsctl | devctl | luigi | kubectl)
            __ansible_install $target
            ;;
        
        golang | az-cli)
            __ansible_install $target --become
            ;;

        *)
            echo "⛔ Program $target cannot be installed by get function."
            ;;
    esac
}

function __ansible_install() {
    program=$1
    sudo_flag=$2
    current_dir=$(pwd)
    cd /home/nikola/.setup

    echo "💽 Installing $program..."

    if [ "$sudo_flag" = "--become" ]; then
        ansible-playbook main.yml --tags "$program" --ask-become-pass
    else
        ansible-playbook main.yml --tags "$program"
    fi
    
    cd $current_dir
}

function __get_kubernetes_prompt() {
    BLUE='\033[0;34m'
    NC='\033[0m' # No Color

    KUBE_CONTEXT="$(kubectl config current-context)"
    KUBE_NAMESPACE="$(kubectl config view --minify --output 'jsonpath={..namespace}')"

    if [ -z "$KUBE_NAMESPACE" ]; then
        KUBE_NAMESPACE="default"
    fi

    KUBE_PROMPT="${BLUE}${KUBE_CONTEXT}/${KUBE_NAMESPACE}${NC}"
    echo -e $KUBE_PROMPT
}

__contains() {
    [[ $1 =~ (^|[[:space:]])$2($|[[:space:]]) ]] && exit 0 || exit 1
}


function __str_contains() {
    str=$1
    x=$2

    if [[ $str == *"$x"* ]]; then
        true
    else
        false
    fi
}
